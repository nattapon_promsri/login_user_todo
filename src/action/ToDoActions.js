export const SEARCH_TODO = 'SEARCH_TODO'
export const FILTER_TODO = 'FILTER_TODO'
export const FETCH_TODOS_BEGIN = 'FETCH_TODOS_BEGIN'
export const FETCH_TODOS_SUCCESS = 'FETCH_TODOS_SUCCESS'
export const FETCH_TODOS_ERROR = 'FETCH_TODOS_ERROR'

export const searchToDo = (value) => {
    return {
        type: SEARCH_TODO,
        payLoad: value
    }
}


export const filterToDo = (value) => {
    return {
        type: FILTER_TODO,
        payLoad: value
    }
}

export const fetchTodos = (id) => {

    var value = id
    return dispatch => {
        dispatch(fetchTodoBegin())
       
        return fetch('https://jsonplaceholder.typicode.com/todos?userId=' + value)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchTodoSuccess(data))
            })
            .catch(error => dispatch(fetchTodoError(error)))
    }
}

export const fetchTodoBegin = () => {
    return {
        type: FETCH_TODOS_BEGIN,
    }
}

export const fetchTodoSuccess = todos => {
    return {
        type: FETCH_TODOS_SUCCESS,
        payLoad: todos
    }
}
export const fetchTodoError = error => {
    return {
        type: FETCH_TODOS_ERROR,
        payLoad: error
    }
}
