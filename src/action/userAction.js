export const FETCH_TODOS_BEGIN = 'FETCH_TODOS_BEGIN'
export const FETCH_TODOS_SUCCESS = 'FETCH_TODOS_SUCCESS'
export const FETCH_TODOS_ERROR = 'FETCH_TODOS_ERROR'
export const SEARCH_USER = 'SEARCH_USER'
export const  DOING_TO_DONE = 'DOING_TO_DONE'


export const fetchUsers = () => {
    return dispatch => {
        dispatch(fetchTodoBegin())
        return  fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(data => {
                dispatch(fetchTodoSuccess(data))
            })
            .catch(error => dispatch(fetchTodoError(error)))
    }
}


export const searchUser = value =>{
    return{
        type: SEARCH_USER,
        payLoad: value
    }
}

export const fetchTodoBegin = () => {
    return {
        type: FETCH_TODOS_BEGIN,
    }
}

export const fetchTodoSuccess = todos => {
    return {
        type: FETCH_TODOS_SUCCESS,
        payLoad: todos
    }
}
export const fetchTodoError = error => {
    return {
        type: FETCH_TODOS_ERROR,
        payLoad: error
    }
}

export const doingToDone = value => {
    return {
        type: DOING_TO_DONE,
        payLoad: value
    }
}