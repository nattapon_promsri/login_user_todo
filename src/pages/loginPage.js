import React from 'react'

const loginPage = () => {
    return (
        <div>
            <center> 
                <h1>LOGIN PAGE</h1>
                <div>
                    <h3>Username : <input type="text" placeholder="username"></input></h3>
                </div>
                <div>
                    <h3>Password : <input type="password" placeholder="password"></input></h3>
                </div>
                <div>
                    <button onClick={() => { window.location = "/processLogin" }}>
                        Login
                    </button>
                </div>
                </center>

        </div >
    )
}

export default loginPage;