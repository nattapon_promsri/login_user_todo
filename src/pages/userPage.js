import React, { useEffect } from 'react'
import  UserList  from '../components/userList'
import  UserSearch  from '../components/userSearch';
import { useDispatch } from 'react-redux';
import { fetchUsers } from '../action/userAction';


const UserPage = () => {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUsers())
    }, [])

    return (

        <div>
            <center>
                <h1>User Page</h1>
                <div>
                    <UserSearch />
                </div>
                <div>
                    <UserList />

                </div>


            </center>
        </div>

    )
}

export default UserPage;