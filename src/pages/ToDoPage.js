import React, { useEffect } from 'react'
import ToDoList from '../components/ToDoList'
import ToDoToolBar from '../components/ToDoToolBar'
import { fetchTodos } from '../action/ToDoActions';
import { useDispatch } from 'react-redux';
const ToDoPage = (props) => {

    const dispatch = useDispatch();

    useEffect(() => {

        const id = props.match.params.id
        dispatch(fetchTodos(id))
    }, [])

    return (

        <div>
            <center>
                <h1>To Do</h1>

                <ToDoToolBar />
                <ToDoList />
            </center>
        </div>

    )
}

export default ToDoPage;