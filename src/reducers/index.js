import { combineReducers } from "redux";
import { todoReducer } from './ToDoReducer'
import { userReducer } from './userReducer'

export const rootReducer = combineReducers({

    todoReducer,
    userReducer

})