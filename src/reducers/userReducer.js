import {FETCH_TODOS_BEGIN,FETCH_TODOS_SUCCESS,FETCH_TODOS_ERROR, SEARCH_USER} from '../action/userAction'


const initialState = {

    searchUser: '',
    loading: false,
    error: '',
    data: []
}

export const userReducer = (state = initialState, action) => {

    switch (action.type) {
        case SEARCH_USER:
            return {
                ...state,
                searchUser: action.payLoad
            }
        case FETCH_TODOS_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_TODOS_SUCCESS:
            return {
                ...state,
                data: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_TODOS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }

        default:
            return state
    }

}