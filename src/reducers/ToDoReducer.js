import { SEARCH_TODO, FILTER_TODO, FETCH_TODOS_BEGIN, FETCH_TODOS_SUCCESS, FETCH_TODOS_ERROR } from '../action/ToDoActions'
import { TODO_FILTER_ALL } from '../components/ToDoToolBar';
import { DOING_TO_DONE } from '../action/userAction'

const initialState = {

    searchText: '',
    filter: TODO_FILTER_ALL,
    loading: false,
    error: '',
    data: []
}

export const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEARCH_TODO:
            return {
                ...state,
                searchText: action.payLoad
            }
        case FILTER_TODO:

            return {
                ...state,
                filter: action.payLoad
            }

        case FETCH_TODOS_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_TODOS_SUCCESS:
            return {
                ...state,
                data: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_TODOS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        case DOING_TO_DONE:
            const newData = [...state.data]
            // action.payLoad  
            newData.forEach(todo => {
                if (action.payLoad == todo.id) {
                    todo.completed = true
                }
            })

            return {
                ...state,
                data: newData
            }
        default:
            return state
    }
}
