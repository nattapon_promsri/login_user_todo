import React from 'react';
import logo from './logo.svg';
import './App.css';
import ToDoPage from './pages/ToDoPage';
import UserPage from './pages/userPage';

function App() {
  return (
    <div >
      {/* <ToDoPage /> */}
      <UserPage />
    </div>
  );
}

export default App;
