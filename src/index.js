import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { rootReducer } from './reducers';
import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import ToDoPage from './pages/ToDoPage';
import userPage from './pages/userPage'
import loginPage from './pages/loginPage';



const composeEnhancers =
    typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        }) : compose;

const enhancer = composeEnhancers(
    applyMiddleware(thunk),
);

const appStore = createStore(rootReducer, enhancer)
ReactDOM.render(<Provider store={appStore}>

    <BrowserRouter>
        <Route path="/processLogin" render={() => {
            localStorage.setItem("isLogin", true);
            return <Redirect to="/userpage" />
        }} />

        <Route path="/processLogout" render={() => {
            localStorage.setItem("isLogin", false);
            return <Redirect to="/login" />
        }} />

        <Route path="/" component={loginPage} exact />
        <Route path="/login" component={loginPage} />
        <Route path="/userpage" component={userPage} />
        <Route path="/todolist/:id" component={ToDoPage} />
    </BrowserRouter>

</Provider>
    , document.getElementById('root'));

serviceWorker.unregister();
