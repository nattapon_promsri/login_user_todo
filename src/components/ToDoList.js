import React from 'react'
import { useSelector, connect, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { filterToDo } from '../action/ToDoActions'
import { TODO_FILTER_ALL } from './ToDoToolBar'
import { doingToDone } from '../action/userAction'



const ToDoList = (props) => {

    const toDoData = useSelector(state => state.todoReducer.data)
    const searchText = useSelector(state => state.todoReducer.searchText)
    const { selectedFilter } = props

    console.log("TCL: ToDoList -> selectedFilter", selectedFilter)

console.log(props)
    const getTodos = () => {
        if (selectedFilter == TODO_FILTER_ALL) {
            return toDoData
        } else {
            return toDoData.filter((value) => {
                return value.completed.toString() == selectedFilter
            })
        }

    }

    const dispatch = useDispatch()
    
    const done = (id) =>{
       dispatch(doingToDone(id))
      
    }


    return (
        <div>
            < table >
                <div>
                    <tr>
                        <th>userId</th>
                        <th>id</th>
                        <th>title</th>
                        <th>Status</th>
                    </tr>
                    {
                        searchText == '' ?

                            getTodos().map((item) => {
                                return (
                                    <tr>
                                        <td>{item.userId}</td>
                                        <td>{item.id}</td>
                                        <td>{item.title}</td>
                                        <td>{
                                            item.completed == true ?
                                                'Done'
                                                :
                                                <button onClick={()=>{done(item.id)}}>Doing</button>
                                        }</td>
                                    </tr>

                                )
                            })
                            :
                            getTodos().filter(m => m.title.match(searchText))
                                .map((item) => {
                                    return (
                                        <tr>
                                            <td>{item.userId}</td>
                                            <td>{item.id}</td>
                                            <td>{item.title}</td>
                                            <td>{
                                                item.completed == true ?
                                                    'Done'
                                                    :
                                                    <button onClick={()=>{done(item.id)}}>Doing</button>
                                            }</td>
                                        </tr>
                                    )

                                })

                    }</div>

            </table >

        </div >
    )
}

const mapStateToProps = state => {
    const props = {
        selectedFilter: state.todoReducer.filter
    }
    return props
}

const mapDispatchToProps = dispatch => {

    return bindActionCreators(
        {
            filterToDo
        }, dispatch
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);