import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { fetchTodos } from '../action/ToDoActions';

const UserList = () => {

    const userData = useSelector(state => state.userReducer.data)
    const userSearch = useSelector(state => state.userReducer.searchUser)

    const dispatch = useDispatch()
    const history = useHistory()

    const onToDo = (id) => {
        history.push("/todolist/" + id)

    }

    return (


        < div >
            <center>

                <div>
                    <table>
                        <div>
                            <tr>
                                <th>ID</th>
                                <th>NAME</th>
                                <th> ToDo</th>
                            </tr>


                            {

                                userSearch == '' ?

                                    userData.map((item) => {
                                        return (
                                            <tr>
                                                <td> {item.id}</td>
                                                <td> {item.name}</td>
                                                <td>{
                                                    <button onClick={() => { onToDo(item.id) }}> Todo</button>
                                                }
                                                </td>
                                            </tr>
                                        )
                                    })

                                    :
                                    userData.filter(m => m.name.match(userSearch))
                                        .map((item) => {
                                            return (
                                                <tr>
                                                    <td> {item.id}</td>
                                                    <td> {item.name}</td>
                                                    <td>{
                                                        <button onClick={() => { onToDo(item.id) }}> Todo</button>
                                                    }
                                                    </td>
                                                </tr>
                                            )
                                        })


                            }


                        </div>


                    </table>
                </div>
                <div>
                    <button className="btn btn-primary" onClick={() => { window.location = "/processLogout" }}>
                        Log Out
                         </button>
                </div>
            </center>
        </div >

    )
}

export default UserList;