import React from 'react'
import { searchUser } from '../action/userAction'
import { useDispatch } from 'react-redux'


const UserSearch = () => {

    const dispatch = useDispatch()
    

    const onSearch = (event) => {
        let value = event.target.value
        dispatch(searchUser(value))
    }

    return (

        <div>

            <div>
                <input onChange={onSearch} />
            </div>

        </div>

    )
}

export default UserSearch;