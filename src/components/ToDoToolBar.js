import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { searchToDo, filterToDo } from '../action/ToDoActions'

export const TODO_FILTER_ALL = -1;


const ToDoToolBar = () => {

    // const [searchToDoTask, setSearchToDoTask] = useState('')

    const dispatch = useDispatch()

    const onSearch = (event) => {
        let value = event.target.value
        console.log("TCL: onSearch -> value", value)
        dispatch(searchToDo(value))
    }

    const onFilter = (event) => {
        let value = event.target.value
        dispatch(filterToDo(value))
    }

    return (
        <div>
            <div>
                <input onChange={onSearch} />
            </div>
            <div>
                <select defaultValue={TODO_FILTER_ALL} onChange={onFilter}>
                    <option value={TODO_FILTER_ALL}>All</option>
                    <option value={false}>Doing</option>
                    <option value={true}>Done</option>
                </select>
            </div>
        </div>

    )
}

export default ToDoToolBar;